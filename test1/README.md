班级：20软工3班 

学号：202010414328

姓名：赵怡



# 实验一：SQL语句的执行计划分析与优化指导 



## 实验目的

 分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。 



##  实验内容 

-  对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。 
-  设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。 



## 实验步骤

### 对hr用户进行授权

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```plaintext
 v_$sesstat, v_$statname 和 v_$session
```



- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

### 设计查询语句

######  查询工作地区为 西雅图 的员工名及工资信息 

- 查询一

```sql
select first_name,last_name,salary,city
from locations,employees,departments
where departments.location_id=locations.location_id
and departments.department_id = employees.department_id
and departments.location_id=1700;



Predicate Information (identified by operation id):        
---------------------------------------------------
 
   1 - access("DEPARTMENTS"."DEPARTMENT_ID"="EMPLOYEES"."DEPARTMENT_ID")
   4 - access("LOCATIONS"."LOCATION_ID"=1700)
   6 - access("DEPARTMENTS"."LOCATION_ID"=1700)
                                                                             

Statistics
-----------------------------------------------------------
               1  CPU used by this session
               5  CPU used when call started
               5  DB time
              37  Requests to/from client
              38  SQL*Net roundtrips to/from client
               8  buffer is not pinned count
              41  buffer is pinned count
             736  bytes received via SQL*Net from client
           70940  bytes sent via SQL*Net to client
               2  calls to get snapshot scn: kcmgss
               4  calls to kcmgcs
              11  consistent gets
               2  consistent gets examination
               2  consistent gets examination (fastpath)
              11  consistent gets from cache
               9  consistent gets pin
               9  consistent gets pin (fastpath)
               2  execute count
               1  index fetch by key
               1  index scans kdiixs1
           90112  logical read bytes from cache
               6  no work - consistent read gets
              38  non-idle wait count
               2  opened cursors cumulative
               2  opened cursors current
               2  parse count (total)
               1  rows fetched via callback
               1  session cursor cache hits
              11  session logical reads
               1  sorts (memory)
            1804  sorts (rows)
              22  table fetch by rowid
               5  table scan blocks gotten
             107  table scan disk non-IMC rows gotten
             107  table scan rows gotten
               1  table scans (short tables)
              38  user calls
```

![1679301471808](C:\Users\86189\Desktop\test1\1679301471808.png)

![1679301008072](C:\Users\86189\AppData\Roaming\Typora\typora-user-images\1679301008072.png)

分析：

该查询通过员工表employees，部门表departments，位置表locations查询工作地区为西雅图的员工名及工资信息 。通过employees，departments表联合查询匹配员工所属部门。之后通过嵌套循环连接匹配departments.location_id=locations.location_id为1700返回出结果。

优化：

可通过子查询预先查找出满足的部分，在用多表联查。这样减少笛卡尔积的大小。





- 查询二

```sql
select first_name,last_name,salary,city
from employees
 join
(select * 
from departments
left join
locations
on departments.location_id=locations.location_id
where departments.location_id=1700)  temp
on employees.department_id=temp.department_id;



Predicate Information (identified by operation id):        
---------------------------------------------------
 
   1 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   2 - access("DEPARTMENTS"."LOCATION_ID"="LOCATIONS"."LOCATION_ID")
   4 - access("DEPARTMENTS"."LOCATION_ID"=1700)
   6 - access("LOCATIONS"."LOCATION_ID"=1700)
 
     

Statistics
-----------------------------------------------------------
               1  DB time
              36  Requests to/from client
              37  SQL*Net roundtrips to/from client
             448  bytes received via SQL*Net from client
           73948  bytes sent via SQL*Net to client
               1  calls to get snapshot scn: kcmgss
               1  calls to kcmgcs
               1  execute count
              37  non-idle wait count
               1  non-idle wait time
               1  opened cursors cumulative
               1  opened cursors current
               1  parse count (total)
               1  session cursor cache hits
               1  sorts (memory)
            1804  sorts (rows)
              37  user calls
```

![1679301278773](C:\Users\86189\AppData\Roaming\Typora\typora-user-images\1679301278773.png)

![](C:\Users\86189\Desktop\test1\1679301213525.png)

分析：

该查询通过员工表employees，部门表departments，位置表locations查询工作地区为西雅图的员工名及工资信息 。首先执行子查询的两表联查匹配出在西雅图的部门，把查询出来的数据作为temp表与employees表联合查询匹配员工所在部门，返回出结果。

sql优化指导：

![1679307486967](C:\Users\86189\AppData\Roaming\Typora\typora-user-images\1679307486967.png)