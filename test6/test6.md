# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414328

姓名：赵怡

班级：20软工3班

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|











## 表及表空间设计方案

 表空间设计：

 表空间1：用于存储系统表、索引和常用的表。

 表空间2：用于存储大型表和历史数据。 

代码如下

```sql
-- 创建表空间
CREATE TABLESPACE tablespace1 DATAFILE '/home/oracle/Desktop/tablespace1.dbf' SIZE 100M;
CREATE TABLESPACE tablespace2 DATAFILE '/home/oracle/Desktop/tablespace2.dbf' SIZE 500M;
```

![](\pic1.png)





表设计： 表1：商品表（products） 字段：商品ID（product_id）、商品名称（product_name）、价格（price）、库存数量（quantity）



 表2：客户表（customers） 字段：客户ID（customer_id）、客户名称（customer_name）、联系方式（contact）、地址（address）



 表3：订单表（orders） 字段：订单ID（order_id）、客户ID（customer_id）、订单日期（order_date）、订单总金额（total_amount）



 表4：订单详情表（order_details） 字段：订单ID（order_id）、商品ID（product_id）、数量（quantity）、单价（unit_price） 

```sql
    -- 创建商品表
    CREATE TABLE products (
      product_id NUMBER PRIMARY KEY,
      product_name VARCHAR2(100),
      price NUMBER,
      quantity NUMBER
    ) TABLESPACE tablespace1;

    -- 创建客户表
    CREATE TABLE customers (
      customer_id NUMBER PRIMARY KEY,
      customer_name VARCHAR2(100),
      contact VARCHAR2(100),
      address VARCHAR2(200)
    ) TABLESPACE tablespace1;

    -- 创建订单表
    CREATE TABLE orders (
      order_id NUMBER PRIMARY KEY,
      customer_id NUMBER,
      order_date DATE,
      total_amount NUMBER,
      CONSTRAINT fk_customer FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ) TABLESPACE tablespace1;

    -- 创建订单详情表
    CREATE TABLE order_details (
      order_id NUMBER,
      product_id NUMBER,
      quantity NUMBER,
      unit_price NUMBER,
      CONSTRAINT pk_order_details PRIMARY KEY (order_id, product_id),
      CONSTRAINT fk_order FOREIGN KEY (order_id) REFERENCES orders(order_id),
      CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES products(product_id)
    ) TABLESPACE tablespace1;
```

![1684464417722](\pic2.png)



## 植入模拟数据

```
-- 插入产品数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, product_name, price, quantity)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;
/

-- 插入客户数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customers (customer_id, customer_name, contact, address)
    VALUES (i, 'Customer ' || i, 'Contact ' || i, 'Address ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 插入订单数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 50000), 0), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0), ROUND(DBMS_RANDOM.VALUE(100, 1000), 2));
  END LOOP;
  COMMIT;
END;
/

-- 插入订单详情数据
DECLARE
  v_order_id NUMBER;
  v_product_id NUMBER;
  v_count NUMBER; -- 添加此行声明v_count变量
BEGIN
  FOR i IN 1..50000 LOOP
    LOOP
      v_order_id := ROUND(DBMS_RANDOM.VALUE(1, 50000), 0);
      v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 50000), 0);

      -- 检查组合是否已存在
      SELECT COUNT(*)
      INTO   v_count
      FROM   order_details
      WHERE  order_id = v_order_id
      AND    product_id = v_product_id;

      -- 如果组合不存在，则插入数据并跳出循环
      IF v_count = 0 THEN
        EXIT;
      END IF;
    END LOOP;

    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (v_order_id, v_product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
  END LOOP;

  COMMIT;
END;
/
```

部分数据图片

![1684466846070](\pic3.png)

![1684466887358](\pic4.png)



## 权限及用户分配方案

```sql
-- 创建管理员用户
CREATE USER salemanager IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE, DBA，CREATE SESSION TO salemanager;

-- 创建普通用户
CREATE USER zy_user IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE TO zy_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO zy_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO zy_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO zy_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO zy_user;
```

![1684467033454](\pic5.png)

##  创建程序包并添加存储过程和函数 

```sql
-- 创建程序包
CREATE PACKAGE sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER);
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER);
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER;
END sales_pkg;
/

CREATE PACKAGE BODY sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(customer_id NUMBER, product_id NUMBER, quantity NUMBER) AS
    price NUMBER;
    total_amount NUMBER;
  BEGIN
    -- 查询商品价格
    SELECT price INTO price FROM products WHERE product_id = product_id;
    
    -- 计算订单总金额
    total_amount := price * quantity;
    
    -- 更新商品库存数量
    UPDATE products SET quantity = quantity - quantity WHERE product_id = product_id;
    
    -- 插入订单表
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (order_id, customer_id, SYSDATE, total_amount);
    
    -- 插入订单详情表
    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (order_id, product_id, quantity, price);
    
    COMMIT;
  END create_order;
  
  -- 存储过程：取消订单
  PROCEDURE cancel_order(order_id NUMBER) AS
  BEGIN
    -- 删除订单详情表中的记录
    DELETE FROM order_details WHERE order_id = order_id;
    
    -- 删除订单表中的记录
    DELETE FROM orders WHERE order_id = order_id;
    
    COMMIT;
  END cancel_order;
  
  -- 函数：计算订单总金额
  FUNCTION calculate_total_amount(order_id NUMBER) RETURN NUMBER AS
    total_amount NUMBER;
  BEGIN
    -- 查询订单详情表中商品的单价和数量，计算订单总金额
    SELECT SUM(quantity * unit_price) INTO total_amount
    FROM order_details
    WHERE order_id = order_id;
    
    RETURN total_amount;
  END calculate_total_amount;
END sales_pkg;
/
```

![1684477310430](\pic6.png)



## 备份方案

- 导出数据库对象：使用 Oracle SQL Developer 中的导出功能，可以选择性地导出数据库对象，如表、视图、函数、存储过程等。这可以作为一种备份方式，以便在需要时能够还原这些对象。
- 导出数据：使用 Oracle SQL Developer 中的数据导出功能，可以导出表中的数据。可以选择导出整个表或根据条件导出特定数据。这样可以备份数据库中的实际数据。
- 定期备份数据文件：使用操作系统级别的备份工具（如 RMAN、文件系统级别的备份工具等），定期备份数据库的数据文件和日志文件。这样可以确保在系统故障或数据丢失时能够进行完全恢复。
- 创建备份脚本：编写 SQL 脚本，在脚本中执行数据备份的 SQL 语句。使用 Oracle SQL Developer 的 SQL Worksheet 或脚本执行功能，可以执行备份脚本并生成备份文件。
- 定期测试恢复：定期测试备份数据的恢复过程，以确保备份的可用性和完整性。可以使用 Oracle SQL Developer 或其他数据库管理工具，将备份数据导入到一个测试数据库中，并验证数据的正确性。
- 存储备份文件：将备份文件存储在安全可靠的位置，例如网络存储设备、磁带库或云存储中。确保备份文件能够长期保存，并采取适当的安全措施，如加密和访问权限控制。
- 监控和日志记录：定期监控备份过程，检查备份是否成功完成。记录备份的执行时间、状态和相关信息，以便后续跟踪和审计。