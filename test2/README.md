##  实验2：用户及权限管理 

-  学号：202010414328，姓名：赵怡，班级：20软件工程3班

###  实验目的 

 掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

###  实验内容 		

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

###  实验步骤 

-  第1步：以system登录到pdborcl，创建角色con_res_zy和用户zy，并授权和分配空间： 

```
$ sqlplus system/123@pdborcl
CREATE ROLE con_res_zy;
GRANT connect,resource,CREATE VIEW TO con_res_zy;
CREATE USER zy IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER zy default TABLESPACE "USERS";
ALTER USER zy QUOTA 50M ON users;
GRANT con_res_zy TO zy;
REVOKE con_res_zy FROM zy;
```

-  第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。 

```
$ sqlplus zy/123@pdborcl
SQL> show user;
USER is "zy"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
```

-  第3步：用户hr连接到pdborcl，查询zy授予它的视图customers_view 

```
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM zy.customers;
elect * from zy.customers
SQL> SELECT * FROM zy.customers_view;
```

-  第4步：设置概要文件

```
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
设置后，sqlplus zy/错误密码@pdborcl 。3次错误密码后，用户被锁定。
锁定后，通过system用户登录，alter user zy unlock命令解锁。
sqlplus system/123@pdborcl
SQL> alter user zy  account unlock;

```